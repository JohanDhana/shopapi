from django.shortcuts import render

# Create your views here.
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Products, CartItem, Cart
from .serializers import CartSerializer


class CartView(generics.GenericAPIView):
    """
    API endpoint that allows carts to be viewed or edited.
    """
    queryset = Cart.objects.all()
    serializer_class = CartSerializer

    def get(self, request):
        customer = self.request.query_params.get('uuid')
        try:
            cart = Cart.objects.filter(customer=customer)
            serializer = CartSerializer(cart[0])

        except Exception as e:
            print(e)
            return Response(e)

        return Response(serializer.data)

    def post(self, request):
        """Add an item to a user's cart.
        """
        serializer = self.get_serializer(data=request.data)
        try:
            product = Products.objects.get(pk=request.data['product_id'])
            quantity = int(request.data['quantity'])
            customer = request.data['customer']
        except Exception as e:
            print(e)
            return Response({'status': 'fail'})

        # Disallow adding to cart if available inventory is not enough
        if product.availability is False:
            print("There  product is not available")
            return Response({'status': 'fail', 'message': 'This  product is not available'})

        existing_cart = Cart.objects.filter(customer=customer)
        if existing_cart:
            cart = existing_cart[0]
        else:
            serializer.is_valid(raise_exception=True)
            cart = serializer.save()

        existing_cart_item = CartItem.objects.filter(cart=cart, product=product).first()
        # before creating a new cart item check if it is in the cart already
        # and if yes increase the quantity of that item
        if existing_cart_item:
            sub_total = (quantity * float(existing_cart_item.product.price))
            # cart
            cart.total_price += sub_total
            cart.save()
            # cart item
            existing_cart_item.sub_total += sub_total
            existing_cart_item.quantity += quantity
            existing_cart_item.save()
        else:
            sub_total = quantity * product.price
            # cart
            cart.total_price += sub_total
            cart.save()
            # cart item
            new_cart_item = CartItem(cart=cart, product=product, quantity=quantity, sub_total=sub_total)
            new_cart_item.save()

        # return the updated cart to indicate success
        serializer = CartSerializer(cart)
        return Response(serializer.data)

    def delete(self, request):
        """Remove an item from a user's cart.
        customers can only remove items from the
        cart 1 at a time, so the quantity of the product to remove from the cart
        will always be 1. If the quantity of the product to remove from the cart
        is 1, delete the cart item. If the quantity is more than 1, decrease
        the quantity of the cart item, but leave it in the cart.
        Parameters
        ----------
        request: request
        Return the updated cart.
        """
        serializer = self.get_serializer(data=request.data)

        customer = request.data['customer']

        try:
            product = Products.objects.get(
                pk=request.data['product_id']
            )
        except Exception as e:
            print(e)
            return Response({'status': 'fail'})

        existing_cart = Cart.objects.filter(customer=customer)
        if existing_cart:
            cart = existing_cart[0]
        else:
            serializer.is_valid(raise_exception=True)
            cart = serializer.save()

        try:
            cart_item = CartItem.objects.get(cart=cart, product=product)
        except Exception as e:
            print(e)
            return Response({'status': 'fail', 'message': 'item not found'})

        # if removing an item where the quantity is 1, remove the cart item
        # completely otherwise decrease the quantity of the cart item
        if cart_item.quantity == 1:
            cart_item.delete()
        else:
            # cart
            cart.total_price-=product.price
            cart.save()
            # cart item
            cart_item.sub_total -= product.price
            cart_item.quantity -= 1
            cart_item.save()

        # return the updated cart to indicate success
        serializer = CartSerializer(cart)
        return Response(serializer.data)
