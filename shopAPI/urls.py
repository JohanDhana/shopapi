
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('', include('api.urls')),
    path('', include('cart.urls')),
    path('', include('accounts.urls')),
    path('', include('front.urls')),
    path('api/auth/', include('accounts.urls')),
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
