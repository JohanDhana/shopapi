from django.contrib import admin
from .models import Business, Products, Images
import admin_thumbnails

# Register your models here.


@admin_thumbnails.thumbnail('image')
class ProductsImageInline(admin.TabularInline):
    model = Images
    readonly_fields = ('id',)
    extra = 1


class ProductAdmin(admin.ModelAdmin):
    inlines = [ProductsImageInline]


admin.site.register(Business)
admin.site.register(Products, ProductAdmin)
