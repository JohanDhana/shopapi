from rest_framework import serializers
from .models import Business, Products, Images


class ImagesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Images
        fields = '__all__'


class ProductsSerializer(serializers.ModelSerializer):
    images = ImagesSerializer(many=True, read_only=True)

    class Meta:
        model = Products
        fields = ['id', 'name', 'price', 'category', 'description', 'availability', 'business', 'images']


class BusinessSerializer(serializers.ModelSerializer):
    products = ProductsSerializer(many=True, read_only=True)

    class Meta:
        model = Business
        fields = ['id', 'name', 'category', 'logo', 'products']
