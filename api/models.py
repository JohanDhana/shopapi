from django.db import models
from django.contrib.auth.models import User


# Create your models here.


class Business(models.Model):
    name = models.CharField(max_length=20)
    category = models.TextField(max_length=10)
    logo = models.ImageField(blank=True, upload_to='images/')
    # remove in prod null=true
    owner = models.ForeignKey(
        User, related_name='business', on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Businesses'


class Products(models.Model):
    name = models.CharField(max_length=20)
    price = models.FloatField(default=0)
    category = models.CharField(max_length=10)
    description = models.TextField(max_length=200)
    availability = models.BooleanField()
    business = models.ForeignKey(
        Business, related_name='products', on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Products'


class Images(models.Model):
    products = models.ForeignKey(Products, related_name='images', on_delete=models.CASCADE, null=True)
    title = models.CharField(max_length=50, blank=True)
    image = models.ImageField(blank=True, upload_to='images/')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = 'Images'
