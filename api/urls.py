from rest_framework import routers
from .viewsets import PublicBusinessViewSet, BusinessViewSet, ProductsViewSet, CartItemViewSet

router = routers.DefaultRouter()
router.register('business-public', PublicBusinessViewSet, 'business-public')
router.register('business', BusinessViewSet, 'business')
router.register('products', ProductsViewSet, 'products')
router.register('cart-item', CartItemViewSet, 'cart-item')

urlpatterns = router.urls
