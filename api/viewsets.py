from cart.models import CartItem
from cart.serializers import CartItemSerializer
from .models import Products, Business
from rest_framework import permissions, viewsets
from .serializers import ProductsSerializer, BusinessSerializer
from rest_framework import filters


class PublicBusinessViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Business.objects.all()
    permission_classes = [permissions.AllowAny]
    serializer_class = BusinessSerializer


class BusinessViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = BusinessSerializer

    def get_queryset(self):
        return self.request.user.business.all()

    def perform_create(self, serializer):
        return serializer.save(owner=self.request.user)


class ProductsViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    serializer_class = ProductsSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['name', 'description']

    def get_queryset(self):
        queryset = Products.objects.all()
        categories = self.request.query_params.get('categories', None)
        if categories is not None:
            queryset = queryset.filter(category=categories)
        return queryset


class CartItemViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows cart items to be viewed or edited.
    """
    queryset = CartItem.objects.all()
    serializer_class = CartItemSerializer
